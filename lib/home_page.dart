import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
        appBar: AppBar(
            title: Container(
                child: Center(
                  child: Text("Isqueiro Virtual v1.0",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                )
            )
        ),
        body: _body(),
      );
  }

  _body() {
    return Container(
      color: Colors.black,
      child: Container(
          child: Center(
              child: _img(),
          )
      ),
    );
  }

  _text() {
    return Text("APPTest", style: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 30
    ));
  }

  _img() {
    //return Image.network("http://www.dopropriobolso.com.br/images/stories/dpb/isqueiro.gif");
    return Image.asset("assets/images/isqueiro.gif",
    width: 600,
    );
  }
}